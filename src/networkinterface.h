#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QNetworkInterface>

class NetworkInterface : public QObject
{
    Q_OBJECT
public:
    explicit NetworkInterface(const QNetworkInterface& networkInterface, QObject *parent = nullptr);
    Q_INVOKABLE QString name() const;
    Q_INVOKABLE QString physicalAddress() const;
    Q_INVOKABLE QString address() const;

private:
    QNetworkInterface m_networkInterface;

signals:

public slots:
};

#endif // NETWORKINTERFACE_H
