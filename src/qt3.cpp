#ifdef QT_QML_DEBUG
#include <QtQuick>
#include "networkinterface.h"
#endif

#include <sailfishapp.h>

int main(int argc, char *argv[])
{
    QGuiApplication *app = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();

    NetworkInterface ni(QNetworkInterface::interfaceFromName("lo"));
    qDebug() << ni.name();

    view->rootContext()->setContextProperty("networkInterface", &ni);

    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    return app->exec();
}
