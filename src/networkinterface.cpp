#include "networkinterface.h"

NetworkInterface::NetworkInterface(const QNetworkInterface& networkInterface, QObject *parent)
    : QObject(parent), m_networkInterface(networkInterface)
{
}

QString NetworkInterface::name() const
{
    return m_networkInterface.name();
}

QString NetworkInterface::physicalAddress() const
{
    return m_networkInterface.hardwareAddress();
}

QString NetworkInterface::address() const
{
    QList<QNetworkAddressEntry> entries = m_networkInterface.addressEntries();
    if (!entries.isEmpty())
        return entries.first().ip().toString();
    else
        return QString();
}
