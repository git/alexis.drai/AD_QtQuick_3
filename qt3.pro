# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = qt3

CONFIG += sailfishapp

SOURCES += src/qt3.cpp \
    src/networkinterface.cpp

DISTFILES += qml/qt3.qml \
    qml/cover/CoverPage.qml \
    rpm/qt3.changes.in \
    rpm/qt3.changes.run.in \
    rpm/qt3.spec \
    translations/*.ts \
    qt3.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/qt3-de.ts

HEADERS += \
    src/networkinterface.h
