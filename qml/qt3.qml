import QtQuick 2.0
import Sailfish.Silica 1.0

ApplicationWindow {
    Column {
        Label {
            text: "Name: " + networkInterface.name()
        }
        Label {
            text: "Physical Address: " + networkInterface.physicalAddress()
        }
        Label {
            text: "IP Address: " + networkInterface.address()
        }
    }
}
